<?php
/**
 * @package      ETD Optimizer
 *
 * @version      2.7.0
 * @copyright    Copyright (C) 2012-2017 ETD Solutions. Tous droits réservés.
 * @license      Apache Version 2 (https://raw.githubusercontent.com/jbanety/etdoptimizer/master/LICENSE.md)
 * @author       ETD Solutions http://www.etd-solutions.com
 **/

function smarty_function_addStyleSheet($params, Smarty_Internal_Template $template) {

    $src      = isset($params['src']) ? trim($params['src']) : '';
    $media    = isset($params['media']) ? trim($params['media']) : 'all';
    $priority = isset($params['priority']) ? $params['priority'] : null;

    if (!empty($src)) {

        // Gestion du cache smarty
        if (Configuration::get('PS_SMARTY_CACHE')) {
            $cache = Cache::getInstance();
            $store_id = EtdOptimizer::getInternalCacheId($template->template_resource . "|" . $src. "|" . $media. "|" . $priority);

            // Si pas encore de le cache
            if (!$cache->exists($store_id)) {

                // On stocke dans le cache
                $cache->set($store_id, [
                    "src" => $src,
                    "media" => $media,
                    "priority" => $priority
                ]);

                // On met à jour le registre
                $registry = $cache->get(CACHE_STYLESHEET_REGISTRY_KEY);
                if ($registry === false) {
                    $registry = [];
                }
                $registry[] = $store_id;
                $cache->set(CACHE_STYLESHEET_REGISTRY_KEY, $registry);
            }

            // On quitte pour ne pas ajouter deux fois
            return;
        }

        EtdOptimizer::addStylesheet($src, $media, $priority);
    }

}
