<?php
/**
 * @package      ETD Optimizer
 *
 * @version      2.7.0
 * @copyright    Copyright (C) 2012-2017 ETD Solutions. Tous droits réservés.
 * @license      Apache Version 2 (https://raw.githubusercontent.com/jbanety/etdoptimizer/master/LICENSE.md)
 * @author       ETD Solutions http://www.etd-solutions.com
 **/

function smarty_function_addMeta($params, Smarty_Internal_Template $template) {

    $name   = isset($params['name']) ? trim($params['name']) : '';
    $content = isset($params['content']) ? trim(str_replace(["\n", "\r"], " ", $params['content'])) : '';

    if (!empty($name)) {

        // Gestion du cache smarty
        if (Configuration::get('PS_SMARTY_CACHE')) {
            $cache = Cache::getInstance();
            $store_id = EtdOptimizer::getInternalCacheId($template->template_resource . ":" . $name . ":" . $content);

            // Si pas encore de le cache
            if (!$cache->exists($store_id)) {

                // On stocke la balise meta dans le cache
                $cache->set($store_id, [
                    "name" => $name,
                    "content" => $content
                ]);

                // On met à jour le registre des metas stockées
                $registry = $cache->get(CACHE_META_REGISTRY_KEY);
                if ($registry === false) {
                    $registry = [];
                }
                $registry[] = $store_id;
                $cache->set(CACHE_META_REGISTRY_KEY, $registry);
            }

            // On quitte pour ne pas ajouter deux la même balise meta.
            return;
        }

        EtdOptimizer::addMeta($name, $content);
    }

}
