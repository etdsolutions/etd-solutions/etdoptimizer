<?php
/**
 * @package      ETD Optimizer
 *
 * @version      2.7.0
 * @copyright    Copyright (C) 2012-2017 ETD Solutions. Tous droits réservés.
 * @license      Apache Version 2 (https://raw.githubusercontent.com/jbanety/etdoptimizer/master/LICENSE.md)
 * @author       ETD Solutions http://www.etd-solutions.com
 **/

function smarty_function_addStyleDeclaration($params, Smarty_Internal_Template $template) {

    $content = isset($params['content']) ? trim($params['content']) : '';
    $type    = isset($params['type']) ? trim($params['type']) : 'text/css';

    if (!empty($content)) {

        // Gestion du cache smarty
        if (Configuration::get('PS_SMARTY_CACHE')) {
            $cache = Cache::getInstance();
            $store_id = EtdOptimizer::getInternalCacheId($template->template_resource . ":" . $content . ":" . $type);

            // Si pas encore de le cache
            if (!$cache->exists($store_id)) {

                // On stocke le style dans le cache
                $cache->set($store_id, [
                    "content" => $content,
                    "type" => $type
                ]);

                // On met à jour le registre des styles stockés
                $registry = $cache->get(CACHE_STYLE_DECLARATION_REGISTRY_KEY);
                if ($registry === false) {
                    $registry = [];
                }
                $registry[] = $store_id;
                $cache->set(CACHE_STYLE_DECLARATION_REGISTRY_KEY, $registry);
            }

            // On quitte pour ne pas ajouter deux le même style.
            return;
        }

        EtdOptimizer::addStyleDeclaration($content, $type);
    }

}
