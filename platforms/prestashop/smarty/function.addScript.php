<?php
/**
 * @package      ETD Optimizer
 *
 * @version      2.7.0
 * @copyright    Copyright (C) 2012-2017 ETD Solutions. Tous droits réservés.
 * @license      Apache Version 2 (https://raw.githubusercontent.com/jbanety/etdoptimizer/master/LICENSE.md)
 * @author       ETD Solutions http://www.etd-solutions.com
 **/

function smarty_function_addScript($params, Smarty_Internal_Template $template) {

    $src      = isset($params['src']) ? trim($params['src']) : '';
    $async    = isset($params['async']) ? (bool) $params['async'] : false;
    $defer    = isset($params['defer']) ? (bool) $params['defer'] : false;
    $priority = isset($params['priority']) ? $params['priority'] : null;

    if (_PS_MODE_DEV_) {
        $r = print_r($params, true);
        EtdOptimizer::$cacheLog[] = "smarty_function_addScript(".$r.", " . $template->template_resource . ")";
    }

    if (!empty($src)) {

        // Gestion du cache smarty
        if (Configuration::get('PS_SMARTY_CACHE')) {
            $cache = Cache::getInstance();
            $store_id = EtdOptimizer::getInternalCacheId($template->template_resource . ":" . $src . ":" . ($async ? "1" : "0") . ":" . ($defer ? "1" : "0") . ":" . $priority);

            // Si pas encore de le cache
            if (!$cache->exists($store_id)) {

                if (_PS_MODE_DEV_) {
                    EtdOptimizer::$cacheLog[] = "Existe pas => $store_id";
                }

                // On stocke le script dans le cache
                $cache->set($store_id, [
                    "src" => $src,
                    "async" => $async,
                    "defer" => $defer,
                    "priority" => $priority
                ]);

                // On met à jour le registre des scripts stockés
                $registry = $cache->get(CACHE_SCRIPT_REGISTRY_KEY);
                if ($registry === false) {
                    $registry = [];
                }
                $registry[] = $store_id;
                $cache->set(CACHE_SCRIPT_REGISTRY_KEY, $registry);
            } elseif (_PS_MODE_DEV_) {
                EtdOptimizer::$cacheLog[] = "Existe => $store_id";
            }

            // On quitte pour ne pas ajouter deux le même script.
            return;
        }

        if (_PS_MODE_DEV_) {
            EtdOptimizer::$cacheLog[] = "Appel EtdOptimizer::addScript";
        }
        EtdOptimizer::addScript($src, $async, $defer, $priority);
    }

}
